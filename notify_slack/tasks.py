from celery import task
from slackclient import SlackClient

from notify_slack.settings import SLACK_API_TOKEN


@task()
def send_slack_notification(course):
    msg = "Course: {} created!".format(course)

    client = SlackClient(SLACK_API_TOKEN)
    client.api_call("chat.postMessage", channel="#openedx", msg=msg)
