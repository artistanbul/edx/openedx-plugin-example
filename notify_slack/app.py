from django.apps import AppConfig

class NotifySlackConfig(AppConfig):
    name = "notify-slack"
    verbose_name = "Notify Slack on course publish"

    plugin_app = {
        u"signals.config": {
            u"cms.djangoapp": {
                u"recievers": [{
                    u'receiver_func_name': u'notify_slack_on_course_publish',
                    u'signal_path': u'notify_slack.signals.COURSE_PUBLISHED',
                    u'dispatch_uid': u'notify_slack_on_course_publish'
                }]
            }
        }
    }
