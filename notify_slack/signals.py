from xmodule.modulestore.django import SignalHandler

from notify_slack.tasks import send_slack_notification


COURSE_PUBLISHED = SignalHandler.course_published


def notify_slack_on_course_publish(sender, course_key, **kwargs):
    send_slack_notification.delay(course_key)
