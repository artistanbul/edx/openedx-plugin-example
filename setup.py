import os
from setuptools import setup, find_packages

setup(
    name='notify-slack',
    version='0.1',
    description='Open edX plugin to notify Slack on course publish',
    packages=find_packages(),
    install_requires=[
        'Django',
        'slackclient'
    ],
    entry_points={
        "cms.djangoapp": [
            "notify_slack = notify_slack.apps:NotifySlackConfig"
        ]
    }
)
