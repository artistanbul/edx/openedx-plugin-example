# Example Open edX Plugin

Example Open edX plugin which notifies Slack on course publish.

## Installation

```
$ sudo su - edxapp -s /bin/bash
$ source venvs/edxapp/bin/activate
$ git clone https://github.com/ArtistanbulPR/openedx-plugin-example.git
$ cd openedx-plugin-example
$ pip install -e .
```
